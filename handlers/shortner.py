# Eva-Maria and Ujjawal

import logging
import aiohttp
import os
from os import environ
from handlers.database import db

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


async def get_short(link, u_id):
    URL = await db.get_shortner(u_id)
    API = await db.get_api(u_id)
    https = link.split(":")[0]
    if "http" == https:
        https = "https"
        link = link.replace("http", https)
    url = f'https://{URL}/api'
    params = {'api': API,
              'url': link,
              }

    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params, raise_for_status=True, ssl=False) as response:
                data = await response.json()
                if data["status"] == "success":
                    return data['shortenedUrl']
                else:
                    logger.error(f"Error: {data['message']}")
                    return f'{link}'

    except Exception as e:
        logger.error(e)
        return f'{link}'
